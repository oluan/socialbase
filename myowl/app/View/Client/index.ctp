<?php echo $this->Form->create('Post', array('url' => 'add', 'class'=>'pure-form')); ?>

<fieldset class="pure-group">
	<?php echo $this->Form->input('message',array('class'=>'pure-input-1-2','placeholder'=>'Qual ideia genial está passando pela sua cabeça agora?','label'=>false, 'maxlength'=>140, 'rows'=>4,'required'=>true, 'id'=>'post')); ?>
</fieldset>
<?php echo $this->Form->submit('Enviar',array('class'=>'pure-button pure-input-1-2 pure-button-primary')) ?>
<?php echo $this->Form->end(); ?>


<!-- A wrapper for all the blog posts -->
<div class="posts">
	<h1 class="content-subhead">TIMELINE</h1>

	<?php foreach ($retorno->posts as $post): ?>
	<section class="post">
		<header class="post-header">
			<?php echo $this->Html->image($post->Post->user->foto, array('alt' => 'Profile','width'=>'40','class'=>'post-avatar')); ?>
			<p class="post-meta">
				By <a href="#" class="post-author"><?php echo $post->Post->user->name; ?></a> em 
				<?php echo $post->Post->datetime; ?>
				
				<?php echo $this->Html->link(("Remover"), array('action' => 'delete', $post->Post->_id),array("class"=>"post-category post-category-js")); ?>
			</p>
		</header>

		<div class="post-description">
			<p>
				<?php echo $post->Post->message; ?>
			</p>
		</div>
	</section>

<?php endforeach; ?>
</div>

<script>

$("#post[maxlength]").each(function() {
	var $this = $(this);
	var maxLength = parseInt($this.attr('maxlength'));
	$this.attr('maxlength', null);

	var el = $("<span class=\"character-count\">" + maxLength + " caracteres restantes.</span>");
	el.insertAfter($this);

	$this.bind('keyup', function() {
		var cc = $this.val().length;

		el.text((maxLength - cc )+ " caracteres restantes.");

		if(maxLength < cc) {
			el.css('color', 'red');
		} else {
			el.css('color', 'green');
		}
	});
});

</script>