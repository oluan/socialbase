<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
	<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">
	<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
	<?php
	echo $this->Html->meta('icon');

	echo $this->Html->css('blog');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
</head>
<body>

	<div id="layout" class="pure-g">
		<div class="sidebar pure-u-1 pure-u-md-1-4">
			<div class="header">
				<?php echo $this->Html->image('myOwl.png', array('alt' => 'LogoMyOwl','width'=>'150')); ?>
				<h1 class="brand-title">MyOwl</h1>
				<h2 class="brand-tagline">Compartilhe seus pensamentos...</h2>


			</div>
		</div>

		<div class="content pure-u-1 pure-u-md-3-4">

					<!-- A single blog post -->
					<?php echo $this->Flash->render(); ?>

					<?php echo $this->fetch('content'); ?>

				<div class="footer">
					<div class="pure-menu pure-menu-horizontal">
						<ul>
							<li class="pure-menu-item"><a href="#">Luan - 2016</a></li>
							<li class="pure-menu-item"><a href="https://www.linkedin.com/in/luan-andrade-5b59a653" class="pure-menu-link">Linkedin</a></li>
							<li class="pure-menu-item"><a href="https://github.com/lsandrade" class="pure-menu-link">Github</a></li>
							<li class="pure-menu-item"><a href="https://bitbucket.org/oluan/" class="pure-menu-link">GitHub</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>


