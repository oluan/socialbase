<?php

App::uses('HttpSocket', 'Network/Http');

class ClientController extends AppController {
    public $components = array('Security', 'RequestHandler');

    var $uses = null;
     

 /**
 * index method
 *
 * @param 
 * @return void
 * @access public
 */

    public function index(){

            $httpSocket = new HttpSocket();         
            $url = "http://".$_SERVER['HTTP_HOST']."/myowl/posts/";
            $retorno = file_get_contents($url);
            //$retorno = $httpSocket->get($url); 
            $this->set('retorno',json_decode($retorno));         
            
    }

 /**
 * add method
 *
 * @param 
 * @return void
 * @access public
 */
     
     
    public function add(){
     
        if ($this->request->is('post')) {
            $link = "http://".$_SERVER['HTTP_HOST']."/myowl/posts/add";
     
            $data = null;
            $httpSocket = new HttpSocket();
            $data['Post']['message'] = $this->request->data['Post']['message'];
            $data['Post']['datetime'] = date("Y/m/d H:i:s"); ;
            $data['Post']['user'] = array('name'=>"Anônimo",'foto'=>'semfoto.jpg');
            $response = $httpSocket->post($link, $data );
            $this->set('response_code', $response->code);
            $this->set('response_body', $response->body);
             
            //$this -> render('/Client');
            $this->redirect(array('controller'=>'Client','action' => 'index'));

        }
    }
     

 /**
 * delete method
 *
 * @param mixed $id null
 * @return void
 * @access public
 */

    public function delete($id = null){
     
       $this->redirect(array('controller'=>'posts','action' => 'delete', $id));
    }
}
        



?>