<?php
/**
 * PostsController class
 *
 * @uses          AppController
 * @package       mongodb
 * @subpackage    mongodb.samples.controllers
 */
class PostsController extends AppController {
	//public $Post;

	public $components = array('RequestHandler');
/**
 * name property
 *
 * @var string 'Posts'
 * @access public
 */
	public $name = 'Posts';
/**
 * index method
 *
 * @return void
 * @access public
 */
	public function index() {
		$this->RequestHandler->renderAs($this, 'json');
		$posts = $this->Post->find('all',array('order'=>array('datetime DESC')));
		$this->set(compact('posts'));
		$this->set('_serialize', array('posts'));
	}


/**
 * View method
 *
 * @return void
 * @access public
 */
	public function view($id = null) {
		$this->RequestHandler->renderAs($this, 'json');

		$posts = $this->Post->find('first', array('conditions' => array('_id' => $id)));
		$this->set(compact('posts'));
		$this->set('_serialize', array('posts'));
	}


/**
 * add method
 *
 * @return void
 * @access public
 */
	public function add() {
		$this->RequestHandler->renderAs($this, 'json');

		$this->Post->create();
		if ($this->Post->save($this->data)) {
			 $message = 'Created';
		} else {
			$message = 'Error';
		}
		$this->set(array(
           	'message' => $message,
           	'_serialize' => array('message')
        ));
	}

/**
 * edit method
 *
 * @param mixed $id null
 * @return void
 * @access public
 */
	public function edit($id = null) {
		$this->RequestHandler->renderAs($this, 'json');

		$this->Post->id = $id;
		if ($this->Post->save($this->data)) {
			 $message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
	}
/**
 * delete method
 *
 * @param mixed $id null
 * @return void
 * @access public
 */
	public function delete($id = null) {
		$this->RequestHandler->renderAs($this, 'json');

		if ($this->Post->delete($id)) {
            $message = 'Deleted';
        } else {
            $message = 'Error';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
        $this->redirect(array('controller'=>'Client','action' => 'index'));
	}

/**
 * deleteall method
 *
 * @return void
 * @access public
 */
	public function deleteall() {
		$conditions = array('title' => 'aa');
		if ($this->Post->deleteAll($conditions)) {
			$this->flash(__('Post deleteAll success', true), array('action' => 'index'));
		} else {
			$this->flash(__('Post deleteAll Fail', true), array('action' => 'index'));
		}
	}
/**
 * updateall method
 *
 * @return void
 * @access public
 */
	public function updateall() {
		$conditions = array('title' => 'ichi2' );
		$field = array('title' => 'ichi' );
		if ($this->Post->updateAll($field, $conditions)) {
			$this->flash(__('Post updateAll success', true), array('action' => 'index'));
		} else {
			$this->flash(__('Post updateAll Fail', true), array('action' => 'index'));
		}
	}
	public function createindex() {
		$mongo = ConnectionManager::getDataSource($this->Post->useDbConfig);
		$mongo->ensureIndex($this->Post, array('title' => 1));
	}
}