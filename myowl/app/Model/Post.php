<?php
class Post extends AppModel {

	var $name = 'Post';
    var $primaryKey = '_id';
    //var $useDbConfig = 'mongo';
 
    function schema() {
        $this->_schema = array(
            '_id' => array('type' => 'string', 'primary' => true, 'length' => 40),
            'message' => array('type' => 'string', 'length' => 140),
            'datetime' => array('type'=>'datetime'),
            'user'=> array('foto'=>array('type'=>'string'),'name'=>array('type'=>'string'))
        );
        return $this->_schema;
    }
}