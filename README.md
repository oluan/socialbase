# Aplicativo MyOwl #
## Microblog para seus pensamentos mais profundos ##
### Desenvolvido por Luan Andrade - 2016 para o desafio da Socialbase ###
![myOwl.png](https://bitbucket.org/repo/5kBGRa/images/2951561625-myOwl.png)

## Tecnologias utilizadas: ##
### Web: ###
* Framework [CakePHP 2.x](http://book.cakephp.org/2.0/pt/index.html)
* Banco de dados [Mongodb](https://www.mongodb.com/)
* HTML5
* CSS
* [Jquery](https://jquery.com/)
* [Purecss](http://purecss.io/)

## Funcionalidades: ##
* Webservice REST que se comunica com a aplicação enviando mensagens JSON
* Postagem de mensagens no microblog
* Conexão entre FW CakePHP e banco de dados Mongodb
* Visualização em tempo real

## Diretórios ##
* /screens - screenshots da aplicação
* /myowl - aplicação web com fw cakephp
* /db - banco de dados exportado do Mongodb em formato JSON

![Login](/screens/myowl01.png?raw=true "Postagem")